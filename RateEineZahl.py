from random import randint


def main():
    # Erzeugt die Zahl welche erraten werden muss
    number_to_guess = randint(0, 100)

    while True:
        # Bittet den Spieler um eine Zahl
        raw_user_input = input("Bitte versuche eine Zahl zwischen"
                               "0 und 100 zu erraten: ")

        # Prüft ob die Zahl eine Zahl ist.
        # Wenn nicht muss der Nutzer eine neue eingeben
        if not raw_user_input.isdigit():
            print("\nBitte gebe nur Zahlen ein!")
            continue

        # Wandelt den String in einen Integer um,
        # damit die Zahl besser veglichen werden kann
        user_input = int(raw_user_input)

        if user_input == number_to_guess:  # Die richtige Zahl wurde erraten
            print("\nHerzlichen Glückwunsch! "
                  "Du hast die richtige Zahl erraten!"
                  "\nDas Programm beendet sich nun.")
            break
        elif user_input < number_to_guess:  # Die Zahl ist zu klein
            print("\nDie Nummer ist leider falsch."
                  "Versuche es doch mit einer größeren Zahl.")
        elif user_input > number_to_guess:  # Die Zahl ist zu groß
            print("\nDie Nummer ist leider falsch."
                  "Versuche es doch mit einer kleineren Zahl.")


if __name__ == "__main__":
    main()
